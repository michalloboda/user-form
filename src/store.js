import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const findIndexByName = (state, name) => state.formData.findIndex(el => el.name === name);
const getValueByIndex = (state, index) => state.formData[index].value;

export default new Vuex.Store({
  state: {
    formData: [
      {
        name: 'phone',
        label: 'phone',
        value: '',
        valid: false,
      },
      {
        name: 'email',
        label: 'email',
        value: '',
        valid: false,
      },
      {
        name: 'name',
        label: 'name',
        value: '',
        valid: false,
      },
      {
        name: 'surname',
        label: 'surname',
        value: '',
        valid: false,
      },
      {
        name: 'street',
        label: 'street',
        value: '',
        valid: false,
      },
      {
        name: 'houseNumber',
        label: 'house number',
        value: '',
        valid: false,
      },
      {
        name: 'city',
        label: 'city',
        value: '',
        valid: false,
      },
      {
        name: 'postcode',
        label: 'postcode',
        value: '',
        valid: false,
      },
    ],
  },
  getters: {
    getFormValue: state => (name) => {
      const index = findIndexByName(state, name);
      return getValueByIndex(state, index);
    },
    disableButton: state => state.formData.some(el => el.valid === false),
  },
  mutations: {
    setFormData(state, payload) {
      const index = findIndexByName(state, payload.name);
      state.formData[index].value = payload.value;
      state.formData[index].valid = payload.valid;
    },
    outputOrder(state) {
      const order = {
        name: getValueByIndex(state, findIndexByName(state, 'name')),
        surname: getValueByIndex(state, findIndexByName(state, 'surname')),
        email: getValueByIndex(state, findIndexByName(state, 'email')),
        phone: getValueByIndex(state, findIndexByName(state, 'phone')),
        address: {
          street: getValueByIndex(state, findIndexByName(state, 'street')),
          houseNumber: getValueByIndex(state, findIndexByName(state, 'houseNumber')),
          city: getValueByIndex(state, findIndexByName(state, 'city')),
          postcode: getValueByIndex(state, findIndexByName(state, 'postcode')),
        },
      };
      console.log(order);
    },
    resetFormData(state) {
      state.formData.forEach((element, index) => {
        state.formData[index].value = '';
        state.formData[index].valid = false;
      });
    },
  },
  actions: {},
});
