import Vue       from 'vue';
import VueRouter from 'vue-router';

import home    from './views/home.view.vue';
import form    from './views/form.view.vue';
import success from './views/success.view.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: home,
  },
  {
    path: '/form',
    name: 'form',
    component: form,
  },
  {
    path: '/success',
    name: 'success',
    component: success,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
